import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import Modal from "react-native-modal";
import { Ionicons } from "@expo/vector-icons";
import { CustomCamera } from "./CustomCamera";
import * as ImagePicker from "expo-image-picker";

/**
 * choose camera has three types
 * none means its a sheet only
 * camera means its opening the camera
 * gallery means its openeing the gallery
 * */

function ChooseMedia({ show, onClosePressed, onPictureTaken }) {
  const [imageTaken, setImageTaken] = useState();
  const [openMediaType, setOpenMediaType] = useState("none");

  ImagePicker.requestMediaLibraryPermissionsAsync();

  const onCameraPress = () => {
    setOpenMediaType("camera");
  };

  const onGalleryPress = () => {
    ImagePicker.launchImageLibraryAsync({ quality: 1, allowsEditing: true })
      .then((response) => {
        console.log(response);
        onPictureTaken(response.uri);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <View style={{ flex: 1 }}>
      {openMediaType === "none" && (
        <Modal isVisible={show} style={{ margin: 0 }}>
          <View style={{ height: "100%", justifyContent: "flex-end" }}>
            <TouchableOpacity
              style={{ alignSelf: "flex-end" }}
              onPress={onClosePressed}
            >
              <Ionicons name={"close-circle"} color={"red"} size={50} />
            </TouchableOpacity>
            <View
              style={{
                backgroundColor: "white",
                height: 300,
                width: "100%",
                justifyContent: "center",
              }}
            >
              <View style={styles.chooseCon}>
                <TouchableOpacity
                  style={styles.chooseIconsCon}
                  onPress={onGalleryPress}
                >
                  <Ionicons name={"image"} color={"white"} size={40} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.chooseIconsCon}
                  onPress={onCameraPress}
                >
                  <Ionicons name={"camera"} color={"white"} size={40} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      )}

      {openMediaType === "camera" && (
        <View style={{ flex: 1 }}>
          <CustomCamera
            onClosePressed={() => {
              setOpenMediaType("none");
            }}
            onPictureTaken={(uri) => {
              onPictureTaken(uri); // run the callback for partent
              setOpenMediaType("none"); // close the camera inside the sheet
            }}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  chooseCon: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  chooseIconsCon: {
    backgroundColor: "orange",
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});

export { ChooseMedia };
