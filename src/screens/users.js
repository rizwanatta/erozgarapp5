import { View, Text, FlatList, Image } from "react-native";
import React, { useState } from "react";
import axios from "axios";

const Users = () => {
  const [usersData, setUsersData] = useState();

  const getDataFromApi = () => {
    axios
      .get("http://192.168.0.103:3000/api/car-brands")
      .then((response) => {
        if (response.data) {
          setUsersData(response.data.brands);
          console.log(response.data);
        }
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  getDataFromApi();
  // use fetch to make an api call to get data from backend of github to list our users

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={usersData}
        renderItem={({ item }) => (
          <View>
            <Text>{item}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default Users;
