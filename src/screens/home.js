import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  TextInput,
  Button,
} from "react-native";
import React, { useState } from "react";
import { CustomButton } from "../components/CustomButton";
import { CustomInput } from "../components/CustomInput";

const imgs = {
  img1: "https://cdn.pixabay.com/photo/2021/06/08/08/55/coffee-6320233_1280.jpg",
  img2: "https://cdn.pixabay.com/photo/2019/07/21/17/00/coffe-4353140_1280.jpg",
};

export default function Home({ navigation }) {
  const [inputValue, setInputValue] = useState();
  const [selectedImage, setSelectedImage] = useState();

  const data = [
    {
      name: "ali",
      rollNumer: 20,
      description: "superb dude",
    },

    {
      name: "ali2",
      rollNumer: 22,
      description: "superb dude 22",
    },

    {
      name: "aslam",
      rollNumer: 23,
      description: "superb dude 23",
    },
  ];

  const goToDetails = () => {
    navigation.navigate("Details", {
      userValue: inputValue,
      selectedImage: selectedImage,
      users: data,
    });
  };

  const myFunc = () => {
    alert("tandha paani peeti hn is leya 100 kg hu");
  };

  return (
    <View style={styles.con}>
      <View>
        <CustomInput
          icon={"lock-closed"}
          placeholderText={"Enter your email"}
        />
        <CustomInput
          icon={"eye-off"}
          placeholderText={"Enter your password"}
          iconColor={"red"}
        />
      </View>
      <Button onPress={goToDetails} title={"GO"} />

      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Details", {
            userValue: inputValue,
            selectedImage: imgs.img1,
            users: data,
          });
        }}
      >
        <Image
          source={{
            uri: imgs.img1,
          }}
          style={{ width: 100, height: 100, margin: 10 }}
        />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Details", {
            userValue: inputValue,
            selectedImage: imgs.img2,
            users: data,
          });
        }}
      >
        <Image
          source={{
            uri: imgs.img2,
          }}
          style={{ width: 100, height: 100, margin: 10 }}
        />
      </TouchableOpacity>

      <CustomButton
        text={"titleli hu ma title hu"}
        textColor={"red"}
        bgColor={"pink"}
        onBtnPress={myFunc}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  con: {
    flex: 1,
  },
});
