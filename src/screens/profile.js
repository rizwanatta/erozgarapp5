import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Button,
  Text,
} from "react-native";
import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedStyle,
  Easing,
} from "react-native-reanimated";
import { useNetInfo } from "@react-native-community/netinfo";

import React, { useEffect, useState } from "react";
import Ionicons from "@expo/vector-icons";
import { ChooseMedia } from "../components/chooseMedia";

const profilePlaceholder = require("../../assets/profile_placeholder.png");

export default function Profile({ route }) {
  // use this to open and close the media picker sheet
  const [showMedia, setShowMedia] = useState(false);
  const [pictureUri, setPictureUri] = useState(""); // if exists use this else use placeholder form above

  const netinfo = useNetInfo();

  // animation necessities
  const randomWidth = useSharedValue(10);
  const config = {
    duration: 500,
    easing: Easing.bezier(0.5, 0.01, 0, 1),
  };
  const style = useAnimatedStyle(() => {
    return {
      width: withTiming(randomWidth.value, config),
    };
  });

  const onImgPressed = () => {
    if (showMedia === true) {
      setShowMedia(false);
    } else {
      setShowMedia(true);
    }
  };

  const handlePictureSource = (source) => {
    setPictureUri(source);
    onImgPressed(); // this will close the bottom sheet after adding the picture
  };

  return (
    <View style={{ flex: 1, marginTop: 40 }}>
      <TouchableOpacity style={styles.imgBtn} onPress={onImgPressed}>
        <Image
          source={pictureUri === "" ? profilePlaceholder : { uri: pictureUri }}
          style={styles.img}
        />
        <Text style={{ color: "red" }}>{}</Text>
      </TouchableOpacity>

      <Animated.View
        style={[
          { width: 100, height: 80, backgroundColor: "black", margin: 30 },
          style,
        ]}
      ></Animated.View>

      <Button
        title="toggle"
        onPress={() => {
          randomWidth.value = Math.random() * 350;
        }}
      />
      <ChooseMedia
        show={showMedia}
        onClosePressed={onImgPressed}
        onPictureTaken={handlePictureSource}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  cameraCon: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  img: {
    height: 150,
    width: 150,
    borderRadius: 75,
    resizeMode: "contain",
  },
  imgBtn: {
    alignSelf: "center",
  },
});
